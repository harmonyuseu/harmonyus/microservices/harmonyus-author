package eu.harmonyus.author.resource;

import eu.harmonyus.author.model.AuthorEntity;
import eu.harmonyus.author.resource.model.Author;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import io.vertx.core.http.HttpHeaders;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;

@QuarkusTest
class AuthorResourceTest {

    @ConfigProperty(name = "quarkus.resteasy.path")
    String restPath;

    @BeforeEach
    void setup() {
        AuthorEntity.deleteAll().await().indefinitely();
    }

    @Test
    @DisplayName("Test - When Calling POST - /api/{version}/authors should create resource - 201")
    void testCreateAuthor() {
        Author author = createAuthor();

        given()
                .when()
                .body(author)
                .contentType(ContentType.JSON)
                .post(restPath + "/authors")
                .then()
                .statusCode(201)
                .header(HttpHeaders.LOCATION.toString(), containsString(restPath + "/authors/john-mathew"))
                .body("email", is("john.mathew@xyz.com"))
                .body("firstname", is("John"))
                .body("lastname", is("Mathew"))
                .body("code", is("john-mathew"));
    }

    @Test
    @DisplayName("Test - When Calling DELETE - /api/{version}/authors/{id} should delete resource - 204")
    void testDeleteAuthor() {
        createAuthorEntity("John","Mathew","John.Mathew@xyz.com");

        given()
                .when()
                .get(restPath + "/authors/john-mathew")
                .then()
                .statusCode(200);

        given()
                .when()
                .delete(restPath + "/authors/john-mathew")
                .then()
                .statusCode(204);

        given()
                .when()
                .get(restPath + "/authors/john-mathew")
                .then()
                .statusCode(404);
    }

    @Test
    @DisplayName("Test - When Calling PUT - /api/{version}/authors/{id} should update resource - 200")
    void testUpdateAuthor() {
        createAuthorEntity("Xohn","Mathew","John.Xathew@xyz.com");

        given()
                .when()
                .get(restPath + "/authors/xohn-mathew")
                .then()
                .statusCode(200);

        Author author = createAuthor();

        given()
                .when()
                .body(author)
                .contentType(ContentType.JSON)
                .put(restPath + "/authors/xohn-mathew")
                .then()
                .statusCode(200);

        given()
                .when()
                .get(restPath + "/authors/john-mathew")
                .then()
                .statusCode(200)
                .body("email", is("john.mathew@xyz.com"))
                .body("firstname", is("John"))
                .body("lastname", is("Mathew"))
                .body("code", is("john-mathew"));
    }

    @Test
    @DisplayName("Test - When Calling GET - /api/{version}/authors should return resources - 200")
    void testGetAll() {
        AuthorEntity authorJohn = createAuthorEntity("John","Mathew","john.mathew@xyz.com");
        AuthorEntity authorJim = createAuthorEntity("Jim","Parker","jim.parker@xyz.com");
        AuthorEntity authorSophia = createAuthorEntity("Sophia","Ran","sophia.ran@xyz.com");

        Author[] authors =  given()
                .when()
                .get(restPath + "/authors")
                .then()
                .statusCode(200)
                .extract()
                .as(Author[].class);
        assertThat("Authors length", authors.length, equalTo(3));
        assertAuthorValue(authors[0], authorJohn, "john-mathew");
        assertAuthorValue(authors[1], authorJim, "jim-parker");
        assertAuthorValue(authors[2], authorSophia, "sophia-ran");
    }

    private void assertAuthorValue(Author author, AuthorEntity authorEntity, String code) {
        assertThat(author.firstname, equalTo(authorEntity.getFirstname()));
        assertThat(author.lastname, equalTo(authorEntity.getLastname()));
        assertThat(author.email, equalTo(authorEntity.getEmail()));
        assertThat(author.code, equalTo(code));
    }

    private AuthorEntity createAuthorEntity(String firstname, String lastname, String email) {
        AuthorEntity authorEntity = new AuthorEntity();
        authorEntity.setFirstname(firstname);
        authorEntity.setLastname(lastname);
        authorEntity.setEmail(email);
        authorEntity.persist().await().indefinitely();
        return authorEntity;
    }

    private Author createAuthor() {
        Author author = new Author();
        author.firstname = "John";
        author.lastname = "Mathew";
        author.email = "John.Mathew@xyz.com";
        author.code = "john-mathew";
        return author;
    }
}
