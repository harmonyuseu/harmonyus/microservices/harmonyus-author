package eu.harmonyus.author.model;

import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.mongodb.panache.reactive.ReactivePanacheMongoEntityBase;
import io.smallrye.mutiny.Uni;
import org.bson.codecs.pojo.annotations.BsonId;

import java.text.Normalizer;
import java.time.LocalDateTime;

@MongoEntity(collection = "author")
public class AuthorEntity extends ReactivePanacheMongoEntityBase {

    @BsonId
    private String code;

    private String firstname;
    private String lastname;
    private String email;
    private String description;
    private LocalDateTime creation = null;
    private LocalDateTime modification = null;

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastName) {
        this.lastname = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public LocalDateTime getCreation() {
        return creation;
    }

    public void setCreation(LocalDateTime creation) {
        this.creation = creation;
    }

    public LocalDateTime getModification() {
        return modification;
    }

    public void setModification(LocalDateTime modification) {
        this.modification = modification;
    }

    @Override
    public Uni<Void> persist() {
        if (this.code == null) {
            this.code = createCode(firstname + " " + lastname);
        }
        if (creation == null) {
            creation = LocalDateTime.now();
        }
        return super.persist();
    }

    @Override
    public Uni<Void> persistOrUpdate() {
        if (modification == null) {
            modification = LocalDateTime.now();
        }
        return super.persistOrUpdate();
    }

    private static String createCode(String value) {
        return stripAccents(value.toLowerCase()).replace(" ", "-").replace("'", "-");
    }

    private static String stripAccents(String s) {
        s = Normalizer.normalize(s, Normalizer.Form.NFD);
        s = s.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
        return s;
    }
}
