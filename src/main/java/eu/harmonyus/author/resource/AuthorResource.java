package eu.harmonyus.author.resource;

import eu.harmonyus.author.resource.mapper.AuthorMapper;
import eu.harmonyus.author.model.AuthorEntity;
import eu.harmonyus.author.resource.model.Author;
import io.quarkus.mongodb.panache.reactive.ReactivePanacheMongoEntityBase;
import io.smallrye.mutiny.Uni;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.time.Duration;
import java.util.List;

@Path("/authors")
@RequestScoped
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class AuthorResource {

    @ConfigProperty(name = "harmonyus.author-resource.get-all.timeout", defaultValue = "2s")
    Duration getAllTimeout;
    @ConfigProperty(name = "harmonyus.author-resource.get-by-code.timeout", defaultValue = "2s")
    Duration getByCodeTimeout;
    @ConfigProperty(name = "harmonyus.author-resource.create.timeout", defaultValue = "2s")
    Duration createTimeout;
    @ConfigProperty(name = "harmonyus.author-resource.modify.timeout", defaultValue = "2s")
    Duration modifyTimeout;
    @ConfigProperty(name = "harmonyus.author-resource.delete.timeout", defaultValue = "2s")
    Duration deleteTimeout;
    @ConfigProperty(name = "harmonyus.author-resource.get-all.max-retries", defaultValue = "1")
    long getAllMaxRetries;
    @ConfigProperty(name = "harmonyus.author-resource.get-by-code.max-retries", defaultValue = "1")
    long getByCodeMaxRetries;
    @ConfigProperty(name = "harmonyus.author-resource.create.max-retries", defaultValue = "1")
    long createMaxRetries;
    @ConfigProperty(name = "harmonyus.author-resource.modify.max-retries", defaultValue = "1")
    long modifyMaxRetries;
    @ConfigProperty(name = "harmonyus.author-resource.delete.max-retries", defaultValue = "1")
    long deleteMaxRetries;

    @GET
    public Uni<List<Author>> getAll() {
        Uni<List<AuthorEntity>> authorEntitiesUni = AuthorEntity.listAll();
        return authorEntitiesUni
                .ifNoItem().after(getAllTimeout).fail()
                .onItem().transform(AuthorMapper::fromEntityList)
                .onFailure().retry().atMost(getAllMaxRetries);
    }

    @GET
    @Path("/{code}")
    public Uni<Author> getByCode(@PathParam("code") String code) {
        Uni<AuthorEntity> authorEntityUni = AuthorEntity.findById(code);
        return authorEntityUni
                .ifNoItem().after(getByCodeTimeout).fail()
                .onItem().ifNull().failWith(NotFoundException::new)
                .map(AuthorMapper::fromEntity)
                .onFailure().retry().atMost(getByCodeMaxRetries);
    }

    @POST
    public Uni<Response> create(Author author) {
        AuthorEntity authorEntity = new AuthorEntity();
        AuthorMapper.toEntity(authorEntity, author);
        return authorEntity.persist()
                .ifNoItem().after(createTimeout).fail()
                .map(unused -> Response.created(URI.create(String.format("/authors/%s", authorEntity.getCode())))
                        .entity(AuthorMapper.fromEntity(authorEntity)).build())
                .onFailure().retry().atMost(createMaxRetries);
    }

    @PUT
    @Path("/{code}")
    public Uni<Response> modify(@PathParam("code") String code, Author author) {
        Uni<AuthorEntity> authorEntityUni = AuthorEntity.findById(code);
        return authorEntityUni
                .ifNoItem().after(modifyTimeout).fail()
                .onItem().ifNull().failWith(NotFoundException::new)
                .flatMap(authorEntity -> {
                    AuthorMapper.toEntity(authorEntity, author);
                    return authorEntity.persistOrUpdate().onItem().transform(unused -> authorEntity);
                })
                .map(authorEntity -> Response.ok().entity(AuthorMapper.fromEntity(authorEntity)).build())
                .onFailure().retry().atMost(modifyMaxRetries);
    }

    @DELETE
    @Path("/{code}")
    public Uni<Response> delete(@PathParam("code") String code) {
        Uni<AuthorEntity> authorEntityUni = AuthorEntity.findById(code);
        return authorEntityUni
                .ifNoItem().after(deleteTimeout).fail()
                .onItem().ifNull().failWith(NotFoundException::new)
                .flatMap(ReactivePanacheMongoEntityBase::delete)
                .map(unused -> Response.noContent().build())
                .onFailure().retry().atMost(deleteMaxRetries);
    }
}