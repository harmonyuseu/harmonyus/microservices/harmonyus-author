package eu.harmonyus.author.resource.mapper;

import eu.harmonyus.author.model.AuthorEntity;
import eu.harmonyus.author.resource.model.Author;

import java.util.ArrayList;
import java.util.List;

public final class AuthorMapper {

    private AuthorMapper() {

    }

    public static Author fromEntity(AuthorEntity authorEntity) {
        Author author = new Author();
        author.code = authorEntity.getCode();
        author.email = authorEntity.getEmail();
        author.firstname = authorEntity.getFirstname();
        author.lastname = authorEntity.getLastname();
        author.description = authorEntity.getDescription();
        return author;
    }

    public static List<Author> fromEntityList(List<AuthorEntity> authorEntities) {
        List<Author> authors = new ArrayList<>();
        authorEntities.forEach(authorEntity -> authors.add(fromEntity(authorEntity)));
        return authors;
    }

    public static void toEntity(AuthorEntity authorEntity, Author author) {
        if (author.code != null) {
            authorEntity.setCode(author.code);
        }
        authorEntity.setEmail(author.email == null ? null : author.email.toLowerCase());
        authorEntity.setFirstname(author.firstname);
        authorEntity.setLastname(author.lastname);
        authorEntity.setDescription(author.description);
    }
}
